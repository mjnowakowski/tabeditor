// Page/animation internal
var g_canvas,
    g_ctx,
    g_frameReq;

var g_cWidth,
    g_cHeight;

// Line display settings (TODO: Set from defaultSettings.json)
var g_numDisplayLines = 2,
    g_numStrings = 6,
    g_interStringWidth = 10,
    g_interLineWidth = 20,
    g_edgeMargin = 10;

// Draw tablature to screen
function renderTab() {
    g_ctx.clearRect(0, 0, g_canvas.width, g_canvas.height);

    // Draw tab lines
    for(var i = 0; i < g_numDisplayLines; i++) {
        // Draw strings
        for(var j = 1; j <= g_numStrings; j++) {
            var xStart = g_edgeMargin;
            var xEnd = g_cWidth - g_edgeMargin;
            var y = g_interStringWidth * j + (i * (g_interLineWidth + g_interStringWidth * g_numStrings));

            g_ctx.save();
            g_ctx.strokeStyle = "#aaaaaa";

            g_ctx.moveTo(xStart, y);
            g_ctx.lineTo(xEnd, y);
            g_ctx.stroke();

            g_ctx.restore();
        }
    }

    g_frameReq = requestAnimationFrame(renderTab); // Next frame
}

// Set canvas width to current width of wrapper (80% clientWidth)
function setCanvasWidth() {
    var wrapper = document.getElementById("wrapper");
    g_cWidth = wrapper.offsetWidth;
    g_canvas.width = g_cWidth;
}

window.addEventListener("load", function() {
    // Setup canvas
    g_canvas = document.getElementById("canvas");
    setCanvasWidth();

    g_ctx = g_canvas.getContext("2d");

    window.addEventListener("resize", setCanvasWidth);

    g_frameReq = requestAnimationFrame(renderTab);
});